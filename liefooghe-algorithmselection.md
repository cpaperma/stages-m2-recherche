#   Apprentissage automatique pour la sélection d'algorithmes d'optimisation

##  Présentation générale

Sujet pas encore affecté.
<!-- Sujet affecté à : ⟨...⟩. -->

### Résumé

Ce projet, mené en collaboration avec l'Université de Shinshu au Japon, vise à
mesurer l'impact des caractéristiques du problème traité (notamment la
dépendance entre les variables) sur sa résolution, et à étudier des modèles
issus de l'apprentissage automatique afin de sélectionner automatiquement
l'algorithme le plus à même de le résoudre efficacement.

### Mots-clés

Algorithmique, optimisation, apprentissage automatique.

### Encadrement

Équipe(s) : Équipe Bonus (CRIStAL / Inria), LIA MODŌ (France / Japon)

Encadrant(s) :
- Arnaud Liefooghe <arnaud.liefooghe@univ-lille.fr>
- Bilel Derbel <bilel.derbel@univ-lille.fr>

[Contacter les encadrants](mailto:arnaud.liefooghe@univ-lille.fr,bilel.derbel@univ-lille.fr?subject=Stage%20de%20recherche).

Localisation : Inria Lille-Nord Europe, Bâtiment Place (EuraTechnologies)


##  Présentation détaillée

### Pré-requis

Algorithmique, optimisation, heuristiques, programmation et conception
orientée-objet (Java, C++, Python), statistique et apprentissage automatique
(R, Python).

### Contexte

Ce projet s'inscrit dans le cadre d'une coopération scientifique entre la France
(CRIStAL, Université de Lille / Inria) et le Japon (Université de Shinshu à
Nagano). Le [LIA MODŌ](http://sites.google.com/view/lia-modo) (Laboratoire
International Associé "Frontiers in Massive Optimization and Computational
Intelligence") fédère une dizaine de chercheurs et jeunes-chercheurs partagés
entre les établissements français et japonais et vise à poser les fondations et
à développer un solveur autonome unifié capable de répondre, de façon globale et
simultanée, aux différents défis soulevés par l'optimisation massive.

### Problématique

Les problèmes d’optimisation d’aujourd’hui sont complexes et caractérisés par la
grande dimensionnalité de leurs variables et objectifs, leur hétérogénéité, et
leur nature coûteuse. En particulier, nous nous intéressons à l'élaboration de
nouvelles techniques d’optimisation, conscientes de l’espace où elles opèrent,
pour la résolution de problèmes d'optimisation larges et variés. Il existe un
grand nombre d'algorithmes issus de l'intelligence artificielle pour
l'optimisation. Cependant, leur efficacité dépend de nombreux facteurs, il
s'avère donc indispensable de les configurer et de les adapter au mieux au
problème à résoudre. Notre objectif est de comprendre les difficultés auxquelles
un algorithme doit faire face, et ce qui le rend efficace ou au contraire
infructueux. Pour cela, une méthodologie basée sur l'analyse de paysage, le
benchmarking, et l'apprentissage automatique nous permettrait non seulement de
comprendre ce qui rend un problème difficile à résoudre, mais aussi de prévoir
les performances de l'algorithme, de sélectionner la configuration la plus
appropriée parmi un ensemble d'algorithmes, et d'en adapter et améliorer la
conception pour la résolution de nouveaux problèmes. En particulier, ce projet
vise à développer divers modèles de dépendance entre les variables, afin de
mesurer l'impact de la difficulté sous-jacente sur la performances des
algorithmes d'optimisation.

### Travail à effectuer

- Cerner les bases de l'optimisation, identifier les principaux algorithmes
d'intelligence artificielle pour l'optimisation, et prendre en main leur
implémentation [ 10% du projet ]

- Analyser et caractériser les instances de la littérature et leurs différences
[ 10% du projet ]

- Développer un modèle de graphe pour representer la dépendance entre les
variables pour ces instances [ 10% du projet ]

- Développer diverses techniques de génération de graphes de dépendance
[ 20% du projet ]

- Expérimenter les algorithmiques et étudier leur comportement et leur
efficacité en fonction des différents modèles de graphe de dépendance [ 20% du
projet ]

- Élaborer un modèle d'apprentissage automatique visant à prédire quel
algorithme utiliser en fonction des différents modèles de graphe de dépendance
[ 20% du projet ]

- Packaging des différents développements et documentation [ 10% du projet ]

### Bibliographie

[1] H. Aguirre, K. Tanaka. Working principles, behavior, and performance of MOEAs on MNK-landscapes. European Journal of Operational Research, vol. 181, n. 3, pp. 1670-1690, 2007
http://doi.org/10.1016/j.ejor.2006.08.004

[2] F. Daolio, A. Liefooghe, S. Verel, H. Aguirre, K. Tanaka. Problem features vs. algorithm performance on rugged multi-objective combinatorial fitness landscapes. Evolutionary Computation, 2017
http://doi.org//10.1162/EVCO_a_00193

[3] S. Kauffman, E. Weinberger. The NK model of rugged fitness landscapes and its application to the maturation of the immune response. Journal of Theoretical Biology, vol. 141 (2), pp. 211–245, 1989
http://doi.org/10.1016/s0022-5193(89)80019-0

[4] A. Liefooghe, B. Derbel, S. Verel, H. Aguirre, K. Tanaka. Towards landscape-aware automatic algorithm configuration: preliminary experiments on neutral and rugged landscapes. European Conference on Evolutionary Computation in Combinatorial Optimisation (EvoCOP 2017), pp 215–232, 2017
http://dx.doi.org/10.1007/978-3-319-55453-2_15

[5] K. Malan, A. Engelbrecht. Fitness Landscape Analysis for Metaheuristic Performance Prediction. Recent Advances in the Theory and Application of Fitness Landscapes, pp. 103-132, 2014
http://doi.org/10.1007/978-3-642-41888-4_4

[6] R. Tintos, D. Whitley, F. Chicano. Partition Crossover for Pseudo-Boolean Optimization. Conference on Foundations of Genetic Algorithms (FOGA 2015), pp. 137-149, 2015
http://doi.org/10.1145/2725494.2725497
